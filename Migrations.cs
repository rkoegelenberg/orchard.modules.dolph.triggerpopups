﻿using Dolph.TriggerPopups.Models;
using Orchard.Core.Settings.Metadata;
using Orchard.ContentManagement.MetaData;
using Orchard.Data.Migration;
using Orchard.Core.Contents.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dolph.TriggerPopups
{
    public class Migrations : DataMigrationImpl
    {

        public int Create()
        {
            ContentDefinitionManager.AlterPartDefinition(
                typeof(TriggerPopupsPart).Name, cfg => cfg.Attachable());

            return 1;
        }
    }
}