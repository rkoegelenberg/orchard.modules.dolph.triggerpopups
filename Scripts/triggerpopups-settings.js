﻿(function ($) {
    var triggerMode = $('#TriggerPopupsSettings_TriggerMode'),
    delayDuration = $('.delay-duration'),
    scrollPercentage = $('.scroll-percentage');

    function setSettingSpecificFieldVisibility() {
        if (triggerMode.val() == "Scroll") {
            scrollPercentage.show();
            delayDuration.hide();
        }
        else if (triggerMode.val() == "Delay") {
            scrollPercentage.hide();
            delayDuration.show();
        }
    }

    triggerMode.on('change', setSettingSpecificFieldVisibility);
    setSettingSpecificFieldVisibility();
})(jQuery);