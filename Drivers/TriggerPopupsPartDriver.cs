﻿using Dolph.TriggerPopups.Models;
using JetBrains.Annotations;
using Orchard;
using Orchard.ContentManagement.Drivers;
using Orchard.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orchard.ContentManagement;
using Orchard.Environment.Extensions;

namespace Dolph.TriggerPopups.Drivers
{
    [UsedImplicitly]
    public class TriggerPopupsPartDriver : ContentPartDriver<TriggerPopupsPart>
    {
        private readonly IOrchardServices _services;

        public TriggerPopupsPartDriver(IOrchardServices services )
        {
            _services = services;
        }

        protected override DriverResult Display(TriggerPopupsPart part, string displayType, dynamic shapeHelper)
        {


            var popupSettings = _services.WorkContext.CurrentSite.As<TriggerPopupsSettingsPart>();
        
            return ContentShape("Parts_TriggerPopups",
                () => shapeHelper.Parts_TriggerPopups(Settings: popupSettings));
        }
    }
}