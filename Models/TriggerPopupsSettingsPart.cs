﻿using Orchard.ContentManagement;
using Orchard.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dolph.TriggerPopups.Models
{

    public class TriggerPopupsSettingsPart : ContentPart
    {        
        public string Text
        {
            get { return this.Retrieve(x => x.Text); }
            set { this.Store(x => x.Text, value); }
        }

        public bool TestMode
        {
            get { return this.Retrieve(x => x.TestMode); }
            set { this.Store(x => x.TestMode, value); }
        }

        public string TriggerMode
        {
            get { return this.Retrieve(x => x.TriggerMode); }
            set { this.Store(x => x.TriggerMode, value); }
        }

        public int DelayDuration
        {
            get { return this.Retrieve(x => x.DelayDuration); }
            set { this.Store(x => x.DelayDuration, value); }
        }

        public int ScrollPercentage
        {
            get { return this.Retrieve(x => x.ScrollPercentage); }
            set { this.Store(x => x.ScrollPercentage, value); }
        }

        public string PopupPosition
        {
            get { return this.Retrieve(x => x.PopupPosition); }
            set { this.Store(x => x.PopupPosition, value); }
        }

        public int CookieExpirationDays
        {
            get { return this.Retrieve(x => x.CookieExpirationDays); }
            set { this.Store(x => x.CookieExpirationDays, value); }
        }
    }
}