﻿using Dolph.TriggerPopups.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Localization;
using Orchard.UI.Notify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dolph.TriggerPopups.Drivers
{
    public class TriggerPopupsSettingsPartDriver : ContentPartDriver<TriggerPopupsSettingsPart>
   {
        public TriggerPopupsSettingsPartDriver(
           INotifier notifier,
           IOrchardServices services)
       {
           _notifier = notifier;
           T = NullLocalizer.Instance;
       }

       public Localizer T { get; set; }
       private const string TemplateName = "Parts/TriggerPopups.Settings";
       private readonly INotifier _notifier;

       protected override DriverResult Editor(TriggerPopupsSettingsPart part, dynamic shapeHelper)
       {
           return ContentShape("Parts_TriggerPopups_Settings",
                   () => shapeHelper.EditorTemplate(
                       TemplateName: TemplateName,
                       Model: part,
                       Prefix: Prefix));
       }

       protected override DriverResult Editor(TriggerPopupsSettingsPart part, IUpdateModel updater, dynamic shapeHelper)
       {
           return Editor(part, shapeHelper);
       }
   }
}