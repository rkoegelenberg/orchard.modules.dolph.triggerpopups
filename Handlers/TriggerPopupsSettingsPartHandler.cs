﻿using Dolph.TriggerPopups.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dolph.TriggerPopups.Handlers
{
    public class TriggerPopupsSettingsPartHandler : ContentHandler
    {
        public Localizer T { get; set; }

        public TriggerPopupsSettingsPartHandler()
        {
            Filters.Add(new ActivatingFilter<TriggerPopupsSettingsPart>("Site"));
            Filters.Add(new TemplateFilterForPart<TriggerPopupsSettingsPart>("TriggerPopupsSettings", "Parts/TriggerPopups.Settings", "triggerpopups"));
        }

        protected override void BuildDisplayShape(BuildDisplayContext context)
        {
            base.BuildDisplayShape(context);

            //((dynamic)context).Layout.Footer.Add(New.User());
        }

        protected override void GetItemMetadata(GetContentItemMetadataContext context)
        {
            if (context.ContentItem.ContentType != "Site")
                return;
            base.GetItemMetadata(context);
            context.Metadata.EditorGroupInfo.Add(new GroupInfo(T("Trigger Popups")) { Id = "TriggerPopups" });
        }
    }
}