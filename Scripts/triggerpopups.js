﻿(function ($) {
    var popup = $('#trp-trigger-popup'),
        closeButton = $('.trp-close-btn'),
        documentHeight = $(document).height(),
        $window = $(window),
        testMode = window.triggerpopups.testMode == 'False' ? false : true;

    closeButton.on('click', function (e) {
        e.preventDefault();
        popup.fadeOut();
    });

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    function setupDelayPopup() {
        setTimeout(function () {
            showPopup();
        }, window.triggerpopups.delayDuration);        
    }

    function setupScrollPopup() {
        $window.on('scroll', function () {
            var scrollTop = $window.scrollTop();
            var trigger = parseInt(window.triggerpopups.scrollPercentage, 10) / 100;            
            if (trigger < (scrollTop / documentHeight)) {
                showPopup();
                $window.off('scroll');
            }
        });
    }

    function showPopup() {        
        if (shouldWeShowPopup()) {
            if (window.triggerpopups.popupPosition === "TopRight" || window.triggerpopups.popupPosition == "BottomRight") {
                popup.fadeIn().animate({
                    right: "+=" + (popup.width() + 10)
                }, 500);
            }
            else {
                popup.fadeIn().animate({
                    left: "+=" + (popup.width() + 10)
                }, 500);
            }
            if (!testMode) {
                setCookie('trp_popup_shown', 1, window.triggerpopups.cookieExpirationDays);
            }
        }
    }

    function shouldWeShowPopup() {
        var result = true;
        if (popup.is(':visible')) {
            result = false;
        }
        else if (!testMode) {
            if (getCookie('trp_popup_shown') != '') {
                result = false;
            }
        }
        return result;
    }

    function initPopup() {
        if (shouldWeShowPopup()) {
            if (window.triggerpopups.triggerMode == "Delay") {
                setupDelayPopup();
            }
            else {
                setupScrollPopup();
            }
            setupPopupPosition();
        }
    }

    function setupPopupPosition() {
        switch (window.triggerpopups.popupPosition) {
            case "TopLeft":
                popup.css('top', 10).css('left', popup.width() * -1);
                break;
            case "TopRight":
                popup.css('top', 10).css('right', popup.width() * -1);
                break;
            case "BottomLeft":
                popup.css('bottom', 10).css('left', popup.width() * -1);
                break;
            case "BottomRight":
                popup.css('bottom', 10).css('right', popup.width() * -1);
                break;
        }
    }

    

    initPopup();
})(jQuery);