using Orchard.UI.Resources;

namespace Orchard.Blogs {
    public class ResourceManifest : IResourceManifestProvider {
        public void BuildManifests(ResourceManifestBuilder builder) {
            var manifest = builder.Add();
            manifest.DefineStyle("TriggerPopups")
                            .SetUrl("triggerpopups.min.css", "triggerpopups.css");

            manifest.DefineScript("TriggerPopups")
                .SetUrl("triggerpopups.js")
                .SetDependencies("jQuery");

            manifest.DefineScript("TriggerPopupsSettings")
                .SetUrl("triggerpopups-settings.js")
                .SetDependencies("jQuery");
        }
    }
}
